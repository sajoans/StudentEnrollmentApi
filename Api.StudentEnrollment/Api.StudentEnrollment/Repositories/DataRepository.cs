﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Web;
using Api.StudentEnrollment.Models;

namespace Api.StudentEnrollment.Repositories
{

    public interface IDataRepository<T>
    {
        T Get(int id);
        void Add(int id, T data);
        IEnumerable<T> GetAll();
    }

    public class DataRepository<T> : IDataRepository<T>
    {
        // in-memory state
        private static ConcurrentDictionary<int, T> _dataDictionary = new ConcurrentDictionary<int, T>();
        

        public DataRepository(ConcurrentDictionary<int, T> seedData = null)
        {
            // Initial test data
            if (seedData != null)
            {
                _dataDictionary = seedData;
            }
        }


        public T Get(int id)
        {
            return _dataDictionary.ContainsKey(id) ? _dataDictionary[id] : default(T);
        }

        public IEnumerable<T> GetAll()
        {
            return _dataDictionary.Values;
        }

        public void Add(int id, T data)
        {
            _dataDictionary.TryAdd(id, data);
        }

    }
}