﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.StudentEnrollment.Controllers
{
    [RoutePrefix("api/Subjects/{subjectId:int}/Lectures")]
    public class LecturesController : ApiController
    {

        private ILectureService _lectureService;

        public LecturesController(ILectureService Lectureservice)
        {
            _lectureService = Lectureservice;
        }

        [HttpGet]
        [Route("{id:int}")]
        public Lecture GetLecture(int subjectId, int id)
        {
            return _lectureService.GetLecture(subjectId,id);
        }


        [HttpGet]
        [Route("")]
        public Lecture[] GetLectures(int subjectId)
        {
            return _lectureService.GetAllLectures(subjectId);
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult AddLecture([FromBody]Lecture lecture, int subjectId)
        {
            _lectureService.AddLecture(subjectId, lecture);
            return Ok();
        }
    }
}
