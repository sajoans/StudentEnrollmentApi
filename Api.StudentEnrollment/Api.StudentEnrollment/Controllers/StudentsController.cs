﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Api.StudentEnrollment.Controllers
{
    [RoutePrefix("api/Students")]
    public class StudentsController : ApiController
    {

        private IStudentService _studentService;
        private IEnrollmentService _enrollmentService;

        public StudentsController(IStudentService studentService, IEnrollmentService enrollmentService)
        {
            _studentService = studentService;
            _enrollmentService = enrollmentService;
        }

        [HttpGet]
        [Route("{id}")]
        public Student GetStudent(int id)
        {
            return _studentService.GetStudent(id);
        }

        [HttpGet]
        [Route("")]
        public Student[] GetStudents()
        {
            return _studentService.GetAllStudents();
        }

        [HttpGet]
        [Route("{id}/Subjects")]
        public Subject[] GetSubjects(int id)
        {
            return _enrollmentService.GetSubjectsEnrolledByStudent(id);
        }


        [HttpPost]
        [Route("{id}/Enroll")]
        public IHttpActionResult Enroll([FromUri]int subjectId, int id)
        {
            if(_enrollmentService.EnrollStudent(id, subjectId))
            {
                return Ok();
            }
            return BadRequest("Rejected");
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult AddStudent([FromBody]Student student)
        {
            _studentService.AddStudent(student);
            return Ok();
        }
    }
}
