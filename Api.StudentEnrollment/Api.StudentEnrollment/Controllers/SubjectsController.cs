﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.StudentEnrollment.Controllers
{
    [RoutePrefix("api/Subjects")]
    public class SubjectsController : ApiController
    {

        private ISubjectService _subjectService;
        private IEnrollmentService _enrollmentService;

        public SubjectsController(ISubjectService SubjectService, IEnrollmentService enrollmentService)
        {
            _subjectService = SubjectService;
            _enrollmentService = enrollmentService;
        }

        [HttpGet]
        [Route("{id}")]
        public Subject GetSubject(int id)
        {
            return _subjectService.GetSubject(id);
        }

        [HttpGet]
        [Route("{id}/Students")]
        public Student[] GetEnrolledStudents(int id)
        {
            return _enrollmentService.GetStudentsEnrolledInSubject(id);
        }

        [HttpGet]
        [Route("")]
        public Subject[] GetSubjects()
        {
            return _subjectService.GetAllSubjects();
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult AddSubject([FromBody]Subject Subject)
        {
            _subjectService.AddSubject(Subject);
            return Ok();
        }



    }
}
