﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.StudentEnrollment.Controllers
{
    [RoutePrefix("api/LectureTheatres")]
    public class LectureTheatresController : ApiController
    {

        private ILectureTheatreService _lectureTheatreService;

        public LectureTheatresController(ILectureTheatreService LectureTheatreService)
        {
            _lectureTheatreService = LectureTheatreService;
        }

        [HttpGet]
        [Route("{id:int}")]
        public LectureTheatre GetLectureTheatre(int id)
        {
            return _lectureTheatreService.GetLectureTheatre(id);
        }

        [HttpGet]
        [Route("")]
        public LectureTheatre[] GetLectureTheatres()
        {
            return _lectureTheatreService.GetAllLectureTheatres();
        }
        
        [HttpPost]
        [Route("")]
        public IHttpActionResult AddLectureTheatre([FromBody]LectureTheatre LectureTheatre)
        {
            _lectureTheatreService.AddLectureTheatre(LectureTheatre);
            return Ok();
        }
    }
}
