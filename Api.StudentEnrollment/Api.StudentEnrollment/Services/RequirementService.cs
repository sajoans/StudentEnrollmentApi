﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.StudentEnrollment.Services
{
    public interface IRequirementService
    {
        bool MeetsStudentWorkloadRequirement(Subject subject, Student student);
        bool MeetsLectureTheatreCapacityRequirement(Subject subject);
    }


    public class RequirementService : IRequirementService
    {
        // TODO: Move to config
        public const int WeeklyHourLimit = 10;

        private IDataRepository<Subject> _subjectRepository;
        private IDataRepository<Lecture> _lectureRepository;
        private IDataRepository<LectureTheatre> _lectureTheatreRepository;


        public RequirementService(IDataRepository<Subject> subjectRepository, IDataRepository<Lecture> lectureRepository, IDataRepository<LectureTheatre> lectureTheatreRepository)
        {
            _subjectRepository = subjectRepository;
            _lectureRepository = lectureRepository;
            _lectureTheatreRepository = lectureTheatreRepository;
        }



        public bool MeetsStudentWorkloadRequirement(Subject subject, Student student)
        {
            // get students curernt work load         
            var allLectures = _subjectRepository.GetAll().Where(x => student.SubjectIds.Contains(x.Id)).Select(x => x.LectureId).ToList();
            decimal totalHours = 0;
            allLectures.ForEach(i =>
            {
                totalHours += (decimal)_lectureRepository.Get(i).DurationInMinutes / 60;
            });

            // Add total workload with new subject
            var newHours = (decimal)_lectureRepository.Get(subject.LectureId).DurationInMinutes / 60;

            return (totalHours + newHours) <= WeeklyHourLimit;

        }

        public bool MeetsLectureTheatreCapacityRequirement(Subject subject)
        {
            var lecture = _lectureRepository.Get(subject.LectureId);
            var lectureTheatre = _lectureTheatreRepository.Get(lecture.LectureTheatreId);

            // Check Capacity
            return subject.EnrolledStudentIds.Count + 1 <= lectureTheatre.Capacity;

        }
    }
}