﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace Api.StudentEnrollment.Services
{

    public interface ILectureService
    {
        Lecture GetLecture(int subjectId, int id);
        void AddLecture(int subjectId, Lecture Lecture);
        Lecture[] GetAllLectures(int subjectId);
    }

    public class LectureService : ILectureService
    {
        private IDataRepository<Lecture> _lectureRepository;


        public LectureService(IDataRepository<Lecture> LectureRepository)
        {
            _lectureRepository = LectureRepository;
        }

        public Lecture GetLecture(int subjectId, int id)
        {
            var result = _lectureRepository.Get(id);
            if (result == null || result.SubjectId != subjectId)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return result;
        }

        public Lecture[] GetAllLectures(int subjectId)
        {
            return _lectureRepository.GetAll().Where(s=>s.SubjectId==subjectId).ToArray();
        }

        public void AddLecture(int subjectId, Lecture Lecture)
        {
            Lecture.SubjectId = subjectId;
            _lectureRepository.Add(Lecture.Id, Lecture);
        }
    }
}