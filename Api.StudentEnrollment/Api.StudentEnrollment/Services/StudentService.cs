﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace Api.StudentEnrollment.Services
{

    public interface IStudentService
    {
        Student GetStudent(int id);
        Student[] GetAllStudents();
        void AddStudent(Student student);

    }

    public class StudentService:IStudentService
    {
        private IDataRepository<Student> _studentRepository;


        public StudentService(IDataRepository<Student> studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public Student GetStudent(int id)
        {
            var result = _studentRepository.Get(id);
            if (result == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return result;
        }

        public Student[] GetAllStudents()
        {
            return _studentRepository.GetAll().ToArray();
        }

        public void AddStudent(Student student)
        {
            _studentRepository.Add(student.Id,student);
        }
    }
}