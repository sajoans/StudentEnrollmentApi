﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace Api.StudentEnrollment.Services
{

    public interface ILectureTheatreService
    {
        LectureTheatre GetLectureTheatre(int id);
        LectureTheatre[] GetAllLectureTheatres();
        void AddLectureTheatre(LectureTheatre LectureTheatre);
    }

    public class LectureTheatreService:ILectureTheatreService
    {
        private IDataRepository<LectureTheatre> _lectureTheatreRepository;


        public LectureTheatreService(IDataRepository<LectureTheatre> LectureTheatreRepository)
        {
            _lectureTheatreRepository = LectureTheatreRepository;
        }

        public LectureTheatre GetLectureTheatre(int id)
        {
            var result = _lectureTheatreRepository.Get(id);
            if (result == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return result;
        }

        public LectureTheatre[] GetAllLectureTheatres()
        {
            return _lectureTheatreRepository.GetAll().ToArray();
        }

        public void AddLectureTheatre(LectureTheatre LectureTheatre)
        {
            _lectureTheatreRepository.Add(LectureTheatre.Id, LectureTheatre);
        }
    }
}