﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace Api.StudentEnrollment.Services
{

    public interface ISubjectService
    {
        Subject GetSubject(int id);
        void AddSubject(Subject Subject);
        Subject[] GetAllSubjects();
    }

    public class SubjectService : ISubjectService
    {
        private IDataRepository<Subject> _subjectRepository;


        public SubjectService(IDataRepository<Subject> SubjectRepository)
        {
            _subjectRepository = SubjectRepository;
        }

        public Subject GetSubject(int id)
        {
            var result = _subjectRepository.Get(id);
            if (result == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return result;
        }

        public Subject[] GetAllSubjects()
        {
            return _subjectRepository.GetAll().ToArray();
        }

        public void AddSubject(Subject Subject)
        {
            _subjectRepository.Add(Subject.Id, Subject);
        }

    }
}