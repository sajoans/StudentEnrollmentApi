﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Api.StudentEnrollment.Services
{
    public interface IEnrollmentService
    {
        Student[] GetStudentsEnrolledInSubject(int subjectId);
        Subject[] GetSubjectsEnrolledByStudent(int studentId);
        bool EnrollStudent(int studentId, int subjectId);
    }


    public class EnrollmentService:IEnrollmentService
    {
        
        private IDataRepository<Student> _studentRepository;
        private IDataRepository<Subject> _subjectRepository;
        private IRequirementService _requirementService;

        public EnrollmentService(IDataRepository<Student> studentRepository, IDataRepository<Subject> subjectRepository, IRequirementService requirementService)
        {
            _studentRepository = studentRepository;
            _subjectRepository = subjectRepository;
            _requirementService = requirementService;
        }


        public Student[] GetStudentsEnrolledInSubject(int subjectId)
        {
            var studentIds = _subjectRepository.Get(subjectId).EnrolledStudentIds;
            return _studentRepository.GetAll().Where(s => studentIds.Contains(s.Id)).ToArray();
        }

        public Subject[] GetSubjectsEnrolledByStudent(int studentId)
        {
           return _subjectRepository.GetAll().Where(s => s.EnrolledStudentIds.Contains(studentId)).ToArray();
        }

        public bool EnrollStudent(int studentId, int subjectId)
        {
            var subject = _subjectRepository.Get(subjectId);
            var student = _studentRepository.Get(studentId);

            if (!IsEligibleForEnrollment(student, subject))
            {
                return false;
            }

            student.SubjectIds.Add(subject.Id);
            subject.EnrolledStudentIds.Add(studentId);

            // Success: Notify Student

            return true;
        }

        private  bool IsEligibleForEnrollment(Student student, Subject subject)
        {
            // Check if Already Enrolled
            if (subject.EnrolledStudentIds.Contains(student.Id))
            {
                // log student already enrolled
                return false;
            }


            return _requirementService.MeetsLectureTheatreCapacityRequirement(subject) && _requirementService.MeetsStudentWorkloadRequirement(subject, student);
            
        }

        
    }
}