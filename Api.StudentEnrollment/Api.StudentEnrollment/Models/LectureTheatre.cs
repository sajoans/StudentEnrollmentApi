﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.StudentEnrollment.Models
{
    public class LectureTheatre
    {
        public int Id;
        public int Capacity;
        public List<int> LectureIds;

    }
}