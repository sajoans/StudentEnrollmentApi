﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.StudentEnrollment.Models
{
    public class Student
    {
        public int Id;
        public string FullName;
        public List<int> SubjectIds;

        public Student()
        {
            SubjectIds = new List<int>();
        }

    }
}