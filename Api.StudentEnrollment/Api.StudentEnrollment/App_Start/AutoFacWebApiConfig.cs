﻿using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Repositories;
using Api.StudentEnrollment.Services;
using Autofac;
using Autofac.Core;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace Api.StudentEnrollment
{
    public class AutoFacWebApiConfig
    {


        public static void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());


            // Initial test data
            var studentSeedData = new ConcurrentDictionary<int, Student>(new Dictionary<int, Student>()
                 {
                    {1, new Student{Id=1,FullName="Sajan Suwal", SubjectIds = new List<int>{ 1, 2 } } },
                    {2, new Student{Id=2,FullName="John Doe"} }
                });
            var studentRepository = new DataRepository<Student>(studentSeedData);
            var studentService = new StudentService(studentRepository);


            builder.Register(ctx => { return studentService; }).As<IStudentService>().SingleInstance();

            // Initial test data
            var SubjectSeedData = new ConcurrentDictionary<int, Subject>(new Dictionary<int, Subject>()
                {
                    {1, new Subject{Id=1,Name="Mathematics", LectureId = 1 , EnrolledStudentIds = new List<int>{1 } } },
                    {2, new Subject{Id=2,Name="Psychology 101",LectureId = 2, EnrolledStudentIds = new List<int>{1 } } }
                });

            var subjectRepository = new DataRepository<Subject>(SubjectSeedData);
            var subjectService = new SubjectService(subjectRepository);
            builder.Register(ctx => { return subjectService; }).As<ISubjectService>().SingleInstance();

            // Initial test data
            var lectureSeedData = new ConcurrentDictionary<int, Lecture>(new Dictionary<int, Lecture>()
                {
                    {1, new Lecture{Id=1, SubjectId = 1, DayOfWeek= 1, DurationInMinutes= 60, LectureTheatreId=1 } },
                    {2, new Lecture{Id=2, SubjectId = 2, DayOfWeek= 2, DurationInMinutes= 60, LectureTheatreId=2} }
                });
            var lectureRepository = new DataRepository<Lecture>(lectureSeedData);
            var lectureService = new LectureService(lectureRepository);
            builder.Register(ctx => { return lectureService; }).As<ILectureService>().SingleInstance();


            // Initial test data
            var lectureTheatreSeedData = new ConcurrentDictionary<int, LectureTheatre>(new Dictionary<int, LectureTheatre>()
                {
                    {1, new LectureTheatre{Id=1,Capacity=5, LectureIds = new List<int>{1 } } },
                    {2, new LectureTheatre{Id=2,Capacity=5, LectureIds = new List<int>{1 }  } }
                });
            var lectureTheatreRepository = new DataRepository<LectureTheatre>(lectureTheatreSeedData);
            var lectureTheatreService = new LectureTheatreService(lectureTheatreRepository);
            builder.Register(ctx => { return lectureTheatreService; }).As<ILectureTheatreService>().SingleInstance();


             builder.Register(ctx =>
            {
                return new EnrollmentService(studentRepository, subjectRepository, new RequirementService(subjectRepository, lectureRepository, lectureTheatreRepository));
            }
            ).As<IEnrollmentService>().InstancePerRequest();


            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }


        
    }
}