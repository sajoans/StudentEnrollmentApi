﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Repositories;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Api.StudentEnrollment.Tests.Repositories.Tests
{
    [TestClass]
    public class DataRepositoryTests
    {
        [TestMethod]
        public void Constructor_Should_Update_Seed_Data()
        {
            // Arrange
            var studentSeedData = new ConcurrentDictionary<int, Student>(new Dictionary<int, Student>()
                 {
                    {1, new Student{Id=1,FullName="Sajan Suwal" } },
                    {2, new Student{Id=2,FullName="John Doe"} }
                });


            // Act
            var testRepository = new DataRepository<Student>(studentSeedData);

            // Assert
            var actualResults = testRepository.GetAll();
            Assert.AreEqual(actualResults.Count(), 2);
            Assert.AreEqual(JsonConvert.SerializeObject(studentSeedData.Values), JsonConvert.SerializeObject(actualResults));
        }

        [TestMethod]
        public void Constructor_Should_CreateEmpty_List_WIth_No_Seed_Data()
        {
            // Arrange
            var reset = new DataRepository<Student>(new ConcurrentDictionary<int, Student>());

            // Act
            var testRepository = new DataRepository<Student>();

            // Assert
            var actualResults = testRepository.GetAll();
            Assert.IsFalse(actualResults.Any());
        }

        [TestMethod]
        public void Get_Should_Get_Item_with_Given_Id()
        {
            // Arrange
            var studentSeedData = new ConcurrentDictionary<int, Student>(new Dictionary<int, Student>()
                 {
                    {1, new Student{Id=1,FullName="Sajan Suwal" } },
                    {2, new Student{Id=2,FullName="John Doe"} }
                });
            var testRepository = new DataRepository<Student>(studentSeedData);

            // Act
            var actualResults = testRepository.Get(1);


            // Assert
            Assert.AreEqual(actualResults.FullName, studentSeedData[1].FullName);
        }
    }
}
