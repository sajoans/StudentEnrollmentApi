﻿using System;
using Api.StudentEnrollment.Controllers;
using NSubstitute;
using Api.StudentEnrollment.Models;
using Newtonsoft.Json;
using Api.StudentEnrollment.Services;
using Ploeh.AutoFixture;
using NUnit.Framework;

namespace Api.LectureEnrollment.Tests.Controllers.Tests
{
    [TestFixture]
    public class LecturesControllerTests
    {

        private Fixture _fixture;


        [SetUp]
        public void init()
        {
            _fixture = new Fixture();
        }


        [Test]
        public void GetLecture_Should_Retern_Lecture_For_Given_Id()
        {
            // Arrange
            var testSubjectId = 123;
            var testLecture = _fixture.Create<Lecture>();
            var mockedLectureService = Substitute.For<ILectureService>();
            mockedLectureService.GetLecture(Arg.Is(testSubjectId), Arg.Is(testLecture.Id)).Returns(testLecture);

            var testController = new LecturesController(mockedLectureService);


            // Act
            var actualResult = testController.GetLecture(testSubjectId, testLecture.Id);

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(testLecture), JsonConvert.SerializeObject(actualResult));
            mockedLectureService.Received(1).GetLecture(Arg.Is(testSubjectId), Arg.Is(testLecture.Id));
        }

        [Test]
        public void AddLecture_Should_AddLecture_Using_LectureService()
        {
            // Arrange
            var testSubjectId = 123;
            var testLecture = _fixture.Create<Lecture>();
            var mockedLectureService = Substitute.For<ILectureService>();
            mockedLectureService.AddLecture(Arg.Is(testSubjectId), Arg.Is(testLecture));
            var testController = new LecturesController(mockedLectureService);


            // Act
            var actualResult = testController.AddLecture(testLecture, testSubjectId);

            // Assert
            mockedLectureService.Received(1).AddLecture(Arg.Is(testSubjectId), Arg.Is(testLecture));
        }

        [Test]
        public void AddLecture_Should_ThrowException_If_SErvice_Fails_toAdd()
        {
            // Arrange
            var testSubjectId = 123;
            var testLecture = _fixture.Create<Lecture>();
            var mockedLectureService = Substitute.For<ILectureService>();
            mockedLectureService
                .When(x => x.AddLecture(testSubjectId, testLecture))
                .Do(x => { throw new Exception(); });

            var testController = new LecturesController(mockedLectureService);


            // Act
            Assert.Throws<Exception>(() => testController.AddLecture(testLecture, testSubjectId));

            // Assert
            //Exception Expected
        }
    }
}
