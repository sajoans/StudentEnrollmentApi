﻿using System;
using Api.StudentEnrollment.Controllers;
using NSubstitute;
using Api.StudentEnrollment.Models;
using Newtonsoft.Json;
using Api.StudentEnrollment.Services;
using Ploeh.AutoFixture;
using NUnit.Framework;

namespace Api.SubjectEnrollment.Tests.Controllers.Tests
{
    [TestFixture]
    public class SubjectsControllerTests
    {

        private Fixture _fixture;


        [SetUp]
        public void init()
        {
            _fixture = new Fixture();
        }


        [Test]
        public void GetSubject_Should_Retern_Subject_For_Given_Id()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();
            var mockedSubjectService = Substitute.For<ISubjectService>();
            mockedSubjectService.GetSubject(Arg.Is(testSubject.Id)).Returns(testSubject);

            var mockedEnrollmentService = Substitute.For<IEnrollmentService>();
            var testController = new SubjectsController(mockedSubjectService, mockedEnrollmentService);


            // Act
            var actualResult = testController.GetSubject(testSubject.Id);

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(testSubject), JsonConvert.SerializeObject(actualResult));
            mockedSubjectService.Received(1).GetSubject(Arg.Is(testSubject.Id));
        }

        [Test]
        public void AddSubject_Should_AddSubject_Using_SubjectService()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();
            var mockedSubjectService = Substitute.For<ISubjectService>();
            mockedSubjectService.AddSubject(Arg.Is(testSubject));

            var mockedEnrollmentService = Substitute.For<IEnrollmentService>();
            var testController = new SubjectsController(mockedSubjectService, mockedEnrollmentService);


            // Act
            var actualResult = testController.AddSubject(testSubject);

            // Assert
            mockedSubjectService.Received(1).AddSubject(Arg.Is(testSubject));
        }

        [Test]
        public void AddSubject_Should_ThrowException_If_SErvice_Fails_toAdd()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();
            var mockedSubjectService = Substitute.For<ISubjectService>();
            mockedSubjectService
                .When(x => x.AddSubject(testSubject))
                .Do(x => { throw new Exception(); });
            

            var mockedEnrollmentService = Substitute.For<IEnrollmentService>();
            var testController = new SubjectsController(mockedSubjectService, mockedEnrollmentService);


            // Act
            Assert.Throws<Exception>(()=> testController.AddSubject(testSubject));

            // Assert
            //Exception Expected
        }
    }
}
