﻿using System;
using Api.StudentEnrollment.Controllers;
using NSubstitute;
using Api.StudentEnrollment.Models;
using Newtonsoft.Json;
using Api.StudentEnrollment.Services;
using Ploeh.AutoFixture;
using NUnit.Framework;

namespace Api.LectureTheatreEnrollment.Tests.Controllers.Tests
{
    [TestFixture]
    public class LectureTheatresControllerTests
    {

        private Fixture _fixture;


        [SetUp]
        public void init()
        {
            _fixture = new Fixture();
        }


        [Test]
        public void GetLectureTheatre_Should_Retern_LectureTheatre_For_Given_Id()
        {
            // Arrange
            var testLectureTheatre = _fixture.Create<LectureTheatre>();
            var mockedLectureTheatreService = Substitute.For<ILectureTheatreService>();
            mockedLectureTheatreService.GetLectureTheatre(Arg.Is(testLectureTheatre.Id)).Returns(testLectureTheatre);

            var testController = new LectureTheatresController(mockedLectureTheatreService);


            // Act
            var actualResult = testController.GetLectureTheatre(testLectureTheatre.Id);

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(testLectureTheatre), JsonConvert.SerializeObject(actualResult));
            mockedLectureTheatreService.Received(1).GetLectureTheatre(Arg.Is(testLectureTheatre.Id));
        }

        [Test]
        public void AddLectureTheatre_Should_AddLectureTheatre_Using_LectureTheatreService()
        {
            // Arrange
            var testLectureTheatre = _fixture.Create<LectureTheatre>();
            var mockedLectureTheatreService = Substitute.For<ILectureTheatreService>();
            mockedLectureTheatreService.AddLectureTheatre(Arg.Is(testLectureTheatre));
            var testController = new LectureTheatresController(mockedLectureTheatreService);


            // Act
            var actualResult = testController.AddLectureTheatre(testLectureTheatre);

            // Assert
            mockedLectureTheatreService.Received(1).AddLectureTheatre(Arg.Is(testLectureTheatre));
        }

        [Test]
        public void AddLectureTheatre_Should_ThrowException_If_SErvice_Fails_toAdd()
        {
            // Arrange
            var testLectureTheatre = _fixture.Create<LectureTheatre>();
            var mockedLectureTheatreService = Substitute.For<ILectureTheatreService>();
            mockedLectureTheatreService
                .When(x => x.AddLectureTheatre(testLectureTheatre))
                .Do(x => { throw new Exception(); });
            
            var testController = new LectureTheatresController(mockedLectureTheatreService);


            // Act
            Assert.Throws<Exception>(()=> testController.AddLectureTheatre(testLectureTheatre));

            // Assert
            //Exception Expected
        }
    }
}
