﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Api.StudentEnrollment.Controllers;
using NSubstitute;
using Api.StudentEnrollment.Services;
using Api.StudentEnrollment.Models;
using Newtonsoft.Json;

namespace Api.StudentEnrollment.Tests.Controllers.Tests
{
    [TestClass]
    public class StudentsControllerTests
    {
        [TestMethod]
        public void GetStudent_Should_Retern_Student_For_Given_Id()
        {
            // Arrange
            var testId = 12;
            var testStudent = new Student { Id = 12, FullName = "Sajan Suwal" };
            var mockedStudentService = Substitute.For<IStudentService>();
            mockedStudentService.GetStudent(Arg.Is(testId)).Returns(testStudent);

            var mockedEnrollmentService = Substitute.For<IEnrollmentService>();
            var testController = new StudentsController(mockedStudentService, mockedEnrollmentService);


            // Act
            var actualResult = testController.GetStudent(testId);

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(testStudent), JsonConvert.SerializeObject(actualResult));
            mockedStudentService.Received(1).GetStudent(Arg.Is(testId));
        }

        [TestMethod]
        public void AddStudent_Should_AddStudent_Using_StudentService()
        {
            // Arrange
            var testStudent = new Student { Id = 12, FullName = "Sajan Suwal" };
            var mockedStudentService = Substitute.For<IStudentService>();
            mockedStudentService.AddStudent(Arg.Is(testStudent));

            var mockedEnrollmentService = Substitute.For<IEnrollmentService>();
            var testController = new StudentsController(mockedStudentService, mockedEnrollmentService);


            // Act
            var actualResult = testController.AddStudent(testStudent);

            // Assert
            mockedStudentService.Received(1).AddStudent(Arg.Is(testStudent));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void AddStudent_Should_ThrowException_If_SErvice_Fails_toAdd()
        {
            // Arrange
            var testStudent = new Student { Id = 12, FullName = "Sajan Suwal" };
            var mockedStudentService = Substitute.For<IStudentService>();
            mockedStudentService
                .When(x => x.AddStudent(testStudent))
                .Do(x => { throw new Exception(); });
            

            var mockedEnrollmentService = Substitute.For<IEnrollmentService>();
            var testController = new StudentsController(mockedStudentService, mockedEnrollmentService);


            // Act
            var actualResult = testController.AddStudent(testStudent);

            // Assert
            //Exception Expected
        }
    }
}
