﻿using System;
using NUnit.Framework;
using Api.StudentEnrollment.Services;
using Api.StudentEnrollment.Repositories;
using Api.StudentEnrollment.Models;
using NSubstitute;
using Ploeh.AutoFixture;
using Newtonsoft.Json;
using System.Web.Http;
using System.Net;
using System.Linq;

namespace Api.StudentEnrollment.Tests.Services.Tests
{
    [TestFixture]
    public class LectureTheatreServiceTests
    {

        private IDataRepository<LectureTheatre> _mockedLectureTheatreRepository;
        private Fixture _fixture;

        [SetUp]
        public void init()
        {
            _mockedLectureTheatreRepository = Substitute.For<IDataRepository<LectureTheatre>>();
            _fixture = new Fixture();
        }

        [Test]
        public void GetLectureTheatre_Should_Get_Correct_LectureTheatre()
        {
            // Arrange
            var testLectureTheatre = _fixture.Create<LectureTheatre>();
            _mockedLectureTheatreRepository.Get(Arg.Is(testLectureTheatre.Id)).Returns(testLectureTheatre);

            var LectureTheatreService = new LectureTheatreService(_mockedLectureTheatreRepository);

            // Act
            var actualResult = LectureTheatreService.GetLectureTheatre(testLectureTheatre.Id);

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(actualResult), JsonConvert.SerializeObject(testLectureTheatre));

        }


        [Test]
        public void GetLectureTheatre_Should_Raise_NotFound_result_if__NO_LectureTheatres_Found_for_Given_Id()
        {
            // Arrange
            _mockedLectureTheatreRepository.Get(Arg.Any<int>()).Returns(default(LectureTheatre));
            var LectureTheatreService = new LectureTheatreService(_mockedLectureTheatreRepository);

            // Act

            // Assert
            HttpResponseException ex = Assert.Throws<HttpResponseException>(() => LectureTheatreService.GetLectureTheatre(09874));

            Assert.AreEqual(ex.Response.StatusCode, HttpStatusCode.NotFound);

        }

        [Test]
        public void AddLectureTheatre_Should_Add_LectureTheatre_To_LectureTheatre()
        {
            // Arrange
            var testLectureTheatre = _fixture.Create<LectureTheatre>();

            var LectureTheatreService = new LectureTheatreService(_mockedLectureTheatreRepository);

            // Act
            LectureTheatreService.AddLectureTheatre(testLectureTheatre);

            // Assert
            _mockedLectureTheatreRepository.Received(1).Add(testLectureTheatre.Id, testLectureTheatre);
        }

        [Test]
        public void GetLectureTheatres_Should_Get_All_LectureTheatres_For_given_LectureTheatre()
        {
            // Arrange
            var testLectureTheatres = _fixture.CreateMany<LectureTheatre>(5).ToList();

            _mockedLectureTheatreRepository.GetAll().Returns(testLectureTheatres);

            var LectureTheatreService = new LectureTheatreService(_mockedLectureTheatreRepository);

            // Act
            var actualResult = LectureTheatreService.GetAllLectureTheatres();

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(actualResult), JsonConvert.SerializeObject(testLectureTheatres));

        }
    }
}
