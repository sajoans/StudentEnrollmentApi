﻿using System;
using NUnit.Framework;
using Api.StudentEnrollment.Services;
using Api.StudentEnrollment.Repositories;
using Api.StudentEnrollment.Models;
using NSubstitute;
using Ploeh.AutoFixture;
using Newtonsoft.Json;
using System.Web.Http;
using System.Net;
using System.Linq;

namespace Api.StudentEnrollment.Tests.Services.Tests
{
    [TestFixture]
    public class StudentServiceTests
    {

        private IDataRepository<Student> _mockedStudentRepository;
        private Fixture _fixture;

        [SetUp]
        public void init()
        {
            _mockedStudentRepository = Substitute.For<IDataRepository<Student>>();
            _fixture = new Fixture();
        }

        [Test]
        public void GetStudent_Should_Get_Correct_Student()
        {
            // Arrange
            var testStudent = _fixture.Create<Student>();
            _mockedStudentRepository.Get(Arg.Is(testStudent.Id)).Returns(testStudent);

            var StudentService = new StudentService(_mockedStudentRepository);

            // Act
            var actualResult = StudentService.GetStudent(testStudent.Id);

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(actualResult), JsonConvert.SerializeObject(testStudent));

        }


        [Test]
        public void GetStudent_Should_Raise_NotFound_result_if__NO_Students_Found_for_Given_Id()
        {
            // Arrange
            _mockedStudentRepository.Get(Arg.Any<int>()).Returns(default(Student));
            var StudentService = new StudentService(_mockedStudentRepository);

            // Act

            // Assert
            HttpResponseException ex = Assert.Throws<HttpResponseException>(() => StudentService.GetStudent(09874));

            Assert.AreEqual(ex.Response.StatusCode, HttpStatusCode.NotFound);

        }

        [Test]
        public void AddStudent_Should_Add_Student_To_Student()
        {
            // Arrange
            var testStudent = _fixture.Create<Student>();

            var StudentService = new StudentService(_mockedStudentRepository);

            // Act
            StudentService.AddStudent(testStudent);

            // Assert
            _mockedStudentRepository.Received(1).Add(testStudent.Id, testStudent);
        }

        [Test]
        public void GetStudents_Should_Get_All_Students_For_given_Student()
        {
            // Arrange
            var testStudents = _fixture.CreateMany<Student>(5).ToList();

            _mockedStudentRepository.GetAll().Returns(testStudents);

            var StudentService = new StudentService(_mockedStudentRepository);

            // Act
            var actualResult = StudentService.GetAllStudents();

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(actualResult), JsonConvert.SerializeObject(testStudents));

        }
    }
}
