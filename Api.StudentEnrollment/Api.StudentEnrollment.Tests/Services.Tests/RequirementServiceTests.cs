﻿using System;
using NSubstitute;
using Api.StudentEnrollment.Repositories;
using Api.StudentEnrollment.Models;
using NUnit.Framework;
using Api.StudentEnrollment.Services;
using Ploeh.AutoFixture;
using System.Collections.Generic;
using System.Linq;

namespace Api.StudentEnrollment.Tests.Services.Tests
{
    [TestFixture]
    public class RequirementServiceTests
    {

        private IDataRepository<Subject> _mockedSubjectRepository;
        private IDataRepository<Lecture> _mockedLectureRepository;
        private IDataRepository<LectureTheatre> _mockedLectureTheatreRepository;
        private Fixture _fixture;


        [SetUp]
        public void Init()
        {
            _mockedSubjectRepository = Substitute.For<IDataRepository<Subject>>();
            _mockedLectureRepository = Substitute.For<IDataRepository<Lecture>>();
            _mockedLectureTheatreRepository = Substitute.For<IDataRepository<LectureTheatre>>();
            _fixture = new Fixture();
        }


        [Test]
        public void MeetsLectureTheatreCapacityRequirement_Should_Return_False_IF_Enrolling_Exceeds_LeactureTheatre_Capacity()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();
            var testLecture = _fixture.Create<Lecture>();
            var testLectureTheatre = _fixture.Create<LectureTheatre>();

            var testCapacity = 5;

            testSubject.LectureId = testLecture.Id;
            // set enrolled students to the lectureTheatre capacity
            testSubject.EnrolledStudentIds = _fixture.CreateMany<int>(testCapacity).ToList();
            testLecture.LectureTheatreId = testLectureTheatre.Id;
            testLectureTheatre.LectureIds.Add(testLecture.Id);
            testLectureTheatre.Capacity = testCapacity;

            _mockedLectureRepository.Get(Arg.Is(testLecture.Id)).Returns(testLecture);
            _mockedLectureTheatreRepository.Get(Arg.Is(testLectureTheatre.Id)).Returns(testLectureTheatre);

            var testRequirementService = new RequirementService(_mockedSubjectRepository, _mockedLectureRepository, _mockedLectureTheatreRepository);


            // Act
            var actualResult = testRequirementService.MeetsLectureTheatreCapacityRequirement(testSubject);

            // Assert
            Assert.IsFalse(actualResult);

        }

        [Test]
        public void MeetsLectureTheatreCapacityRequirement_Should_Return_TRUE_IF_Under_LeactureTheatre_Capacity()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();
            var testLecture = _fixture.Create<Lecture>();
            var testLectureTheatre = _fixture.Create<LectureTheatre>();

            var testCapacity = 5;

            testSubject.LectureId = testLecture.Id;
            // set enrolled students to the lectureTheatre capacity minus 1
            testSubject.EnrolledStudentIds = _fixture.CreateMany<int>(testCapacity - 1).ToList();
            testLecture.LectureTheatreId = testLectureTheatre.Id;
            testLectureTheatre.LectureIds.Add(testLecture.Id);
            testLectureTheatre.Capacity = testCapacity;

            _mockedLectureRepository.Get(Arg.Is(testLecture.Id)).Returns(testLecture);
            _mockedLectureTheatreRepository.Get(Arg.Is(testLectureTheatre.Id)).Returns(testLectureTheatre);

            var requirementService = new RequirementService(_mockedSubjectRepository, _mockedLectureRepository, _mockedLectureTheatreRepository);


            // Act
            var actualResult = requirementService.MeetsLectureTheatreCapacityRequirement(testSubject);

            // Assert
            Assert.IsTrue(actualResult);
            _mockedLectureRepository.Received(1).Get(Arg.Is(testLecture.Id));
            _mockedLectureTheatreRepository.Received(1).Get(Arg.Is(testLectureTheatre.Id));

        }

        [Test]
        public void MeetsStudentWorkloadRequirement_Should_Return_FALSE_IF_MaxHours_Allowed_For_Student_Reached()
        {
            // Arrange
            var testStudent = _fixture.Create<Student>();
            var testSubjects = _fixture.CreateMany<Subject>(2);
            var testLecture = _fixture.Create<Lecture>();
            testLecture.DurationInMinutes = RequirementService.WeeklyHourLimit * 60 / 2;

            testStudent.SubjectIds = testSubjects.Select(s => s.Id).ToList();
            testSubjects.ToList().ForEach(s => { s.LectureId = testLecture.Id; });


            var testLectureTheatre = _fixture.Create<LectureTheatre>();

            _mockedLectureRepository.Get(Arg.Is(testLecture.Id)).Returns(testLecture);
            _mockedSubjectRepository.GetAll().Returns(testSubjects);

            var testSubject = _fixture.Create<Subject>();

            testSubject.LectureId = testLecture.Id;
            _mockedSubjectRepository.Get(Arg.Is(testSubject.Id)).Returns(testSubject);

            var testRequirementService = new RequirementService(_mockedSubjectRepository, _mockedLectureRepository, _mockedLectureTheatreRepository);


            // Act
            var actualResult = testRequirementService.MeetsStudentWorkloadRequirement(testSubject, testStudent);

            // Assert
            Assert.IsFalse(actualResult);
            _mockedLectureRepository.Received(3).Get(Arg.Is(testLecture.Id));
            _mockedSubjectRepository.Received(1).GetAll();

        }

        [Test]
        public void MeetsStudentWorkloadRequirement_Should_Return_TRUE_IF_MaxHours_Allowed_For_Student__NOT_Reached()
        {
            // Arrange
            var testStudent = _fixture.Create<Student>();
            var testSubjects = _fixture.CreateMany<Subject>(2);
            var testLecture = _fixture.Create<Lecture>();

            // Make  total enrolled lecture time below the allowed max
            testLecture.DurationInMinutes = RequirementService.WeeklyHourLimit * 60 / 3;

            testStudent.SubjectIds = testSubjects.Select(s => s.Id).ToList();
            testSubjects.ToList().ForEach(s => { s.LectureId = testLecture.Id; });


            var testLectureTheatre = _fixture.Create<LectureTheatre>();

            _mockedLectureRepository.Get(Arg.Is(testLecture.Id)).Returns(testLecture);
            _mockedSubjectRepository.GetAll().Returns(testSubjects);

            var testSubject = _fixture.Create<Subject>();
            testSubject.LectureId = testLecture.Id;
            _mockedSubjectRepository.Get(Arg.Is(testSubject.Id)).Returns(testSubject);

            var testRequirementService = new RequirementService(_mockedSubjectRepository, _mockedLectureRepository, _mockedLectureTheatreRepository);


            // Act
            var actualResult = testRequirementService.MeetsStudentWorkloadRequirement(testSubject, testStudent);

            // Assert
            Assert.IsTrue(actualResult);
            _mockedLectureRepository.Received(3).Get(Arg.Is(testLecture.Id));
            _mockedSubjectRepository.Received(1).GetAll();

        }
    }
}
