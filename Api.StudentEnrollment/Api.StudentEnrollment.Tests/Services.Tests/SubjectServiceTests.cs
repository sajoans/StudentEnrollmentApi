﻿using System;
using NUnit.Framework;
using Api.StudentEnrollment.Services;
using Api.StudentEnrollment.Repositories;
using Api.StudentEnrollment.Models;
using NSubstitute;
using Ploeh.AutoFixture;
using Newtonsoft.Json;
using System.Web.Http;
using System.Net;
using System.Linq;

namespace Api.StudentEnrollment.Tests.Services.Tests
{
    [TestFixture]
    public class SubjectServiceTests
    {

        private IDataRepository<Subject> _mockedSubjectRepository;
        private Fixture _fixture;

        [SetUp]
        public void init()
        {
            _mockedSubjectRepository = Substitute.For<IDataRepository<Subject>>();
            _fixture = new Fixture();
        }

        [Test]
        public void GetSubject_Should_Get_Correct_Subject()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();
            _mockedSubjectRepository.Get(Arg.Is(testSubject.Id)).Returns(testSubject);

            var SubjectService = new SubjectService(_mockedSubjectRepository);

            // Act
            var actualResult = SubjectService.GetSubject(testSubject.Id);

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(actualResult), JsonConvert.SerializeObject(testSubject));

        }


        [Test]
        public void GetSubject_Should_Raise_NotFound_result_if__NO_Subjects_Found_for_Given_Id()
        {
            // Arrange
            _mockedSubjectRepository.Get(Arg.Any<int>()).Returns(default(Subject));
            var SubjectService = new SubjectService(_mockedSubjectRepository);

            // Act

            // Assert
            HttpResponseException ex = Assert.Throws<HttpResponseException>(() => SubjectService.GetSubject(09874));

            Assert.AreEqual(ex.Response.StatusCode, HttpStatusCode.NotFound);

        }

        [Test]
        public void AddSubject_Should_Add_Subject_To_Subject()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();

            var SubjectService = new SubjectService(_mockedSubjectRepository);

            // Act
            SubjectService.AddSubject(testSubject);

            // Assert
            _mockedSubjectRepository.Received(1).Add(testSubject.Id, testSubject);
        }

        [Test]
        public void GetSubjects_Should_Get_All_Subjects_For_given_subject()
        {
            // Arrange
            var testSubjects = _fixture.CreateMany<Subject>(5).ToList();

            _mockedSubjectRepository.GetAll().Returns(testSubjects);

            var SubjectService = new SubjectService(_mockedSubjectRepository);

            // Act
            var actualResult = SubjectService.GetAllSubjects();

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(actualResult), JsonConvert.SerializeObject(testSubjects));

        }
    }
}
