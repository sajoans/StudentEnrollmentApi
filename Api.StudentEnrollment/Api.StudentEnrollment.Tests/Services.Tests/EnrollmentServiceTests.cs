﻿using System;
using Api.StudentEnrollment.Models;
using Api.StudentEnrollment.Repositories;
using NSubstitute;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Api.StudentEnrollment.Services;
using System.Collections.Generic;

namespace Api.StudentEnrollment.Tests.Services.Tests
{
    [TestFixture]
    public class EnrollmentServiceTests
    {
        private IDataRepository<Student> _mockedStudentRepository;
        private IDataRepository<Subject> _mockedSubjectRepository;
        private IRequirementService _mockedRequirementService;
        private Fixture _fixture;


        [SetUp]
        public void Init()
        {
            _mockedSubjectRepository = Substitute.For<IDataRepository<Subject>>();
            _mockedStudentRepository = Substitute.For<IDataRepository<Student>>();
            _mockedRequirementService = Substitute.For<IRequirementService>();
            _fixture = new Fixture();
        }


        [Test]
        public void EnrollStudent_Should_Return_True_IF_Enroll_Successful()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();
            var testStudent = _fixture.Create<Student>();
            // ensure student not enrolled in subject
            testSubject.EnrolledStudentIds = new List<int>();
            _mockedStudentRepository.Get(Arg.Is(testStudent.Id)).Returns(testStudent);
            _mockedSubjectRepository.Get(Arg.Is(testSubject.Id)).Returns(testSubject);
            _mockedRequirementService.MeetsStudentWorkloadRequirement(Arg.Is(testSubject), Arg.Is(testStudent)).Returns(true);
            _mockedRequirementService.MeetsLectureTheatreCapacityRequirement(Arg.Is(testSubject)).Returns(true);

            var testEnrollmentService = new EnrollmentService(_mockedStudentRepository, _mockedSubjectRepository, _mockedRequirementService);

            // Act
            var actualResult = testEnrollmentService.EnrollStudent(testStudent.Id, testSubject.Id);

            // Assert
            Assert.IsTrue(actualResult);
        }

        [Test]
        public void EnrollStudent_Should_Return_FALSE_IF_Enroll__NOT_Successful_DueTo_TheatreCapacity()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();
            var testStudent = _fixture.Create<Student>();
            // ensure student not enrolled in subject
            testSubject.EnrolledStudentIds = new List<int>();
            _mockedStudentRepository.Get(Arg.Is(testStudent.Id)).Returns(testStudent);
            _mockedSubjectRepository.Get(Arg.Is(testSubject.Id)).Returns(testSubject);
            _mockedRequirementService.MeetsStudentWorkloadRequirement(Arg.Is(testSubject), Arg.Is(testStudent)).Returns(true);
            _mockedRequirementService.MeetsLectureTheatreCapacityRequirement(Arg.Is(testSubject)).Returns(false);

            var testEnrollmentService = new EnrollmentService(_mockedStudentRepository, _mockedSubjectRepository, _mockedRequirementService);

            // Act
            var actualResult = testEnrollmentService.EnrollStudent(testStudent.Id, testSubject.Id);

            // Assert
            Assert.IsFalse(actualResult);
        }

        [Test]
        public void EnrollStudent_Should_Return_FALSE_IF_Enroll__NOT_Successful_DueTo_Student_WorkLoad()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();
            var testStudent = _fixture.Create<Student>();
            // ensure student not enrolled in subject
            testSubject.EnrolledStudentIds = new List<int>();
            _mockedStudentRepository.Get(Arg.Is(testStudent.Id)).Returns(testStudent);
            _mockedSubjectRepository.Get(Arg.Is(testSubject.Id)).Returns(testSubject);
            _mockedRequirementService.MeetsStudentWorkloadRequirement(Arg.Is(testSubject), Arg.Is(testStudent)).Returns(false);
            _mockedRequirementService.MeetsLectureTheatreCapacityRequirement(Arg.Is(testSubject)).Returns(true);

            var testEnrollmentService = new EnrollmentService(_mockedStudentRepository, _mockedSubjectRepository, _mockedRequirementService);

            // Act
            var actualResult = testEnrollmentService.EnrollStudent(testStudent.Id, testSubject.Id);

            // Assert
            Assert.IsFalse(actualResult);
        }

        [Test]
        public void EnrollStudent_Should_Return_FALSE_IF_Enroll__NOT_Successful_DueTo_Student_AlreadyEnrolled()
        {
            // Arrange
            var testSubject = _fixture.Create<Subject>();
            var testStudent = _fixture.Create<Student>();
            // enroll student to subject
            testSubject.EnrolledStudentIds.Add(testStudent.Id);

            _mockedStudentRepository.Get(Arg.Is(testStudent.Id)).Returns(testStudent);
            _mockedSubjectRepository.Get(Arg.Is(testSubject.Id)).Returns(testSubject);
            _mockedRequirementService.MeetsStudentWorkloadRequirement(Arg.Is(testSubject), Arg.Is(testStudent)).Returns(true);
            _mockedRequirementService.MeetsLectureTheatreCapacityRequirement(Arg.Is(testSubject)).Returns(true);

            var testEnrollmentService = new EnrollmentService(_mockedStudentRepository, _mockedSubjectRepository, _mockedRequirementService);

            // Act
            var actualResult = testEnrollmentService.EnrollStudent(testStudent.Id, testSubject.Id);

            // Assert
            Assert.IsFalse(actualResult);
        }
    }
}
