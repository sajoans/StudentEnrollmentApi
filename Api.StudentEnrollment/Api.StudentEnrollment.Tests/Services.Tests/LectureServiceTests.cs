﻿using System;
using NUnit.Framework;
using Api.StudentEnrollment.Services;
using Api.StudentEnrollment.Repositories;
using Api.StudentEnrollment.Models;
using NSubstitute;
using Ploeh.AutoFixture;
using Newtonsoft.Json;
using System.Web.Http;
using System.Net;
using System.Linq;

namespace Api.StudentEnrollment.Tests.Services.Tests
{
    [TestFixture]
    public class LectureServiceTests
    {

        private IDataRepository<Lecture> _mockedLectureRepository;
        private Fixture _fixture; 

        [SetUp]
        public void init()
        {
            _mockedLectureRepository = Substitute.For<IDataRepository<Lecture>>();
            _fixture = new Fixture();
        }

        [Test]
        public void GetLecture_Should_Get_Correct_Lecture()
        {
            // Arrange
            var testLecture = _fixture.Create<Lecture>();
            testLecture.SubjectId = 12;
            _mockedLectureRepository.Get(Arg.Is(testLecture.Id)).Returns(testLecture);

            var lectureService = new LectureService(_mockedLectureRepository);

            // Act
            var actualResult = lectureService.GetLecture(testLecture.SubjectId, testLecture.Id);

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(actualResult), JsonConvert.SerializeObject(testLecture));

        }

        [Test]
        public void GetLecture_Should_Raise_NotFound_result_if_Lectures_Found_with_Different_subjectId()
        {
            // Arrange
            var testLecture = _fixture.Create<Lecture>();
            testLecture.SubjectId = 12;
            _mockedLectureRepository.Get(Arg.Is(testLecture.Id)).Returns(testLecture);

            var lectureService = new LectureService(_mockedLectureRepository);

            // Act

            // Assert
            HttpResponseException ex = Assert.Throws<HttpResponseException>(() => lectureService.GetLecture(2, testLecture.Id));
               
            Assert.AreEqual(ex.Response.StatusCode, HttpStatusCode.NotFound);            

        }

        [Test]
        public void GetLecture_Should_Raise_NotFound_result_if__NO_Lectures_Found_for_Given_Id()
        {
            // Arrange
            var testLecture = _fixture.Create<Lecture>();
            _mockedLectureRepository.Get(Arg.Is(testLecture.Id)).Returns(testLecture);

            var lectureService = new LectureService(_mockedLectureRepository);

            // Act

            // Assert
            HttpResponseException ex = Assert.Throws<HttpResponseException>(() => lectureService.GetLecture(2, 09874));

            Assert.AreEqual(ex.Response.StatusCode, HttpStatusCode.NotFound);

        }

        [Test]
        public void AddLecture_Should_Add_Lecture_To_Subject()
        {
            // Arrange
            var testLecture = _fixture.Create<Lecture>();
            var testSubjectId = 12;            

            var lectureService = new LectureService(_mockedLectureRepository);

            // Act
            lectureService.AddLecture(testSubjectId, testLecture);

            // Assert
            testLecture.SubjectId = testSubjectId;
            _mockedLectureRepository.Received(1).Add(testLecture.Id, testLecture);
        }

        [Test]
        public void GetLectures_Should_Get_All_Lectures_For_given_subject()
        {
            // Arrange
            var testSubjectId = 12;
            var testLecturesWithTestSubject = _fixture.CreateMany<Lecture>(5).ToList();
            testLecturesWithTestSubject.ForEach(s => { s.SubjectId = testSubjectId; });

            var testLectures = _fixture.CreateMany<Lecture>(5).ToList();
            testLectures.AddRange(testLecturesWithTestSubject);

            _mockedLectureRepository.GetAll().Returns(testLectures);

            var lectureService = new LectureService(_mockedLectureRepository);

            // Act
            var actualResult = lectureService.GetAllLectures(testSubjectId);

            // Assert
            Assert.AreEqual(JsonConvert.SerializeObject(actualResult), JsonConvert.SerializeObject(testLecturesWithTestSubject));

        }
    }
}
